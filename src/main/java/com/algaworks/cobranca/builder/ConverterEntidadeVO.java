package com.algaworks.cobranca.builder;

import java.util.ArrayList;
import java.util.List;

import com.algaworks.cobranca.model.Titulo;
import com.algaworks.cobranca.vo.TituloVO;

public class ConverterEntidadeVO {

	public static Titulo converterTituloVOParaTitulo(TituloVO tituloVO) {
		Titulo tituloEntidade = new Titulo();

		tituloEntidade.setId(tituloVO.getId());
		tituloEntidade.setDescricao(tituloVO.getDescricao());
		tituloEntidade.setDataVencimento(tituloVO.getDataVencimento());
		tituloEntidade.setValor(tituloVO.getValor());
		tituloEntidade.setStatus(tituloVO.getStatus());
		
		return tituloEntidade;
	}
	
	public static TituloVO converterTituloParaTituloVO(Titulo titulo) {
		TituloVO tituloVO = new TituloVO();

		tituloVO.setId(titulo.getId());
		tituloVO.setDescricao(titulo.getDescricao());
		tituloVO.setDataVencimento(titulo.getDataVencimento());
		tituloVO.setValor(titulo.getValor());
		tituloVO.setStatus(titulo.getStatus());
		
		return tituloVO;
	}
	
	public static List<TituloVO> converterListTituloParaListaTituloVO(List<Titulo> listTitulo){
		List<TituloVO> listaTituloVO = new ArrayList<>();
		if(listTitulo != null && !listTitulo.isEmpty()){
			for(Titulo tituloEntidade: listTitulo){
				TituloVO tituloVO = converterTituloParaTituloVO(tituloEntidade);
				listaTituloVO.add(tituloVO);
			}
		}
		return listaTituloVO;
	}

}

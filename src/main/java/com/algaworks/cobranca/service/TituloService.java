package com.algaworks.cobranca.service;

import java.util.List;

import com.algaworks.cobranca.exception.BusinessException;
import com.algaworks.cobranca.vo.TituloFiltro;
import com.algaworks.cobranca.vo.TituloVO;

public interface TituloService {
	
	public void salvar(TituloVO titulo) throws BusinessException;
	
	public List<TituloVO> listarTodosTitulos();
	
	public TituloVO consultarPorId(Long id);
	
	public void excluir(Long id);
	
	public String receber(Long id);
	
	public List<TituloVO> consultaTituloPorDescricao(TituloFiltro filtro);

		

}

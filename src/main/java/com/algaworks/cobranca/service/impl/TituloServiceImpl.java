package com.algaworks.cobranca.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.algaworks.cobranca.builder.ConverterEntidadeVO;
import com.algaworks.cobranca.dao.TituloDAO;
import com.algaworks.cobranca.exception.BusinessException;
import com.algaworks.cobranca.model.StatusTitulo;
import com.algaworks.cobranca.model.Titulo;
import com.algaworks.cobranca.service.TituloService;
import com.algaworks.cobranca.vo.TituloFiltro;
import com.algaworks.cobranca.vo.TituloVO;

@Service
public class TituloServiceImpl implements TituloService{

	@Autowired
	private TituloDAO tituloDAO;
	
	@Override
	public void salvar(TituloVO titulo) throws BusinessException {
		try{
			tituloDAO.save(ConverterEntidadeVO.converterTituloVOParaTitulo(titulo));
		}catch (DataIntegrityViolationException e) {
			throw new BusinessException("Formato de data inválido!");
		}
		
	}

	@Override
	public List<TituloVO> listarTodosTitulos() {
		List<TituloVO> listTituloVO= ConverterEntidadeVO.converterListTituloParaListaTituloVO(tituloDAO.findAll());
		return listTituloVO;
	}

	@Override
	public TituloVO consultarPorId(Long id) {
		TituloVO tituloVO = ConverterEntidadeVO.converterTituloParaTituloVO(tituloDAO.findOne(id));
		return tituloVO;
	}

	@Override
	public void excluir(Long id) {
		Titulo titulo = tituloDAO.findOne(id);
		tituloDAO.delete(titulo);
		
	}

	@Override
	public String receber(Long id) {
		Titulo titulo = tituloDAO.findOne(id);
		titulo.setStatus(StatusTitulo.RECEBIDO);
		tituloDAO.save(titulo);
		return StatusTitulo.RECEBIDO.getDescricao();
	}

	@Override
	public List<TituloVO> consultaTituloPorDescricao(TituloFiltro filtro) {
		String descricao = filtro.getDescricao() == null ? "%" : filtro.getDescricao();
		List<TituloVO> listTituloVO= 
				ConverterEntidadeVO.converterListTituloParaListaTituloVO(tituloDAO.findByDescricaoContaining(descricao));
		return listTituloVO;
	}
	

}

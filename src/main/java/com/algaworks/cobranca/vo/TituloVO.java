package com.algaworks.cobranca.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import com.algaworks.cobranca.model.StatusTitulo;

import lombok.Data;

@Data
public class TituloVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long id;
	
	@NotEmpty(message = "O campo descrição deve ser preenchido.")
	private String descricao;
	
	@NotNull(message="O campo data de vencimento deve ser preenchida.")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataVencimento;
	
	@NotNull(message="O campo valor deve ser preenchido.")
	@DecimalMin(value="0.01" , message="O valor não pode ser menos que 0,01")
	@NumberFormat(pattern="#,##0.00")
	private BigDecimal valor;
	
	private StatusTitulo status;
	
	public boolean isPendente(){
		return StatusTitulo.PENDENTE.equals(this.getStatus());
	}

}

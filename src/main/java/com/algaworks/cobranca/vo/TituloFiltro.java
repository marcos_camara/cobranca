package com.algaworks.cobranca.vo;

import lombok.Data;

@Data
public class TituloFiltro {

	private String descricao;
	
}

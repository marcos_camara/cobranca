package com.algaworks.cobranca.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.algaworks.cobranca.exception.BusinessException;
import com.algaworks.cobranca.model.StatusTitulo;
import com.algaworks.cobranca.service.TituloService;
import com.algaworks.cobranca.utils.Constantes;
import com.algaworks.cobranca.vo.TituloFiltro;
import com.algaworks.cobranca.vo.TituloVO;

@Controller
@RequestMapping(Constantes.PATH_TITULO)
public class TituloController {

	private static final String CADASTRO_VIEW = "cadastroTitulo";

	@Autowired
	private TituloService tituloService;

	@RequestMapping("/novo")
	public ModelAndView novo() {
		ModelAndView modelView = new ModelAndView(CADASTRO_VIEW);
		modelView.addObject(new TituloVO());
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String salvar(@Validated TituloVO tituloVO, Errors erros, RedirectAttributes attibutes) {

		String retorno = null;
		if (erros.hasErrors()) {
			retorno = CADASTRO_VIEW;
		} else {

			try {
				tituloService.salvar(tituloVO);
				attibutes.addFlashAttribute("mensagem", "Titulo salvo com sucesso!");
				retorno = "redirect:/titulos/novo";
			} catch (BusinessException e) {
				erros.rejectValue("dataVencimento", null, e.getMessage());
				retorno = CADASTRO_VIEW;
			}
		}
		return retorno;
	}

	@RequestMapping
	public ModelAndView pesquisar(@ModelAttribute("filtro") TituloFiltro filtro) {
		List<TituloVO> listaTodosTitulos = tituloService.consultaTituloPorDescricao(filtro);
		ModelAndView modelView = new ModelAndView("pesquisarTitulo");
		modelView.addObject("listTodosTitulos", listaTodosTitulos);
		return modelView;
	}
	
	@ModelAttribute("listStatus")
	private List<StatusTitulo> listStatusTitulo() {
		return Arrays.asList(StatusTitulo.values());
	}

	@RequestMapping("{id}")
	public ModelAndView edicao(@PathVariable Long id) {
		TituloVO tituloVo = tituloService.consultarPorId(id);
		ModelAndView modelView = new ModelAndView(CADASTRO_VIEW);
		modelView.addObject(tituloVo);

		return modelView;
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public String exclusao(@PathVariable Long id, RedirectAttributes attributes) {
		tituloService.excluir(id);
		attributes.addFlashAttribute("mensagem", "Titulo excluido com sucesso!");
		return "redirect:/titulos";
	}

	@RequestMapping(value = "/{id}/receber", method = RequestMethod.PUT)
	public @ResponseBody String receber(@PathVariable Long id) {
		return tituloService.receber(id);
	}

}

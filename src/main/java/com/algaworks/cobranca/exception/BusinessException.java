package com.algaworks.cobranca.exception;

public class BusinessException extends Exception{

	private static final long serialVersionUID = 1L;

	public BusinessException(String mensagemErro){
		super(mensagemErro);
	}
	
}

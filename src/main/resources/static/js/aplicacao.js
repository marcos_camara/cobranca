$('#confirmacaoExclusaoModal').on('show.bs.modal', function(event){
	
	//Pega o botão que disparou o evento
	var button = $(event.relatedTarget);
	
	//
	var codTitulo = button.data('id');
	var descricaoTitulo = button.data('descricao');
	
	//Pega o modal
	var modal = $(this);
	
	//Pega o form
	var form = modal.find('form');
	
	//Pega a action do form
	var action = form.data('url-base');
	
	//Verifica se a action já tem uma barra no final
	if(!action.endsWith('/')){
		action += '/';
	}
	form.attr('action', action + codTitulo);
	
	modal.find('.modal-body span').html('Tem certeza que deseja excluir o título <strong>' + descricaoTitulo + '</strong>?');
	
});

$(function(){
	
	//Corrige problemas de tooltip
	$('[rel="tooltip"]').tooltip();
	
	//Formtar todos os campos com monetário para a class js-currencys
	$('.js-currency').maskMoney({decimal: ',', thousands: '.', allowZero: true});
	
	$('.js-atualizar-status').on('click',function(event){
		event.preventDefault();
		
		var buttonReceber = $(event.currentTarget);
		var urlReceber = buttonReceber.attr('href');
		
		var response = $.ajax({
			url: urlReceber,
			type: 'PUT'
		});
		
		response.done(function(e) {
			var idTitulo = buttonReceber.data('id');
			$('[data-role=' + idTitulo + ']').html('<span class="label label-success">' + e + '</span>')
			buttonReceber.hide();
		});
		
		response.fail(function() {
			
		});

	});
});
